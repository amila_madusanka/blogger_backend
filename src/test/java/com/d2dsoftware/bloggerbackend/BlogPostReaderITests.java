package com.d2dsoftware.bloggerbackend;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.d2dsoftware.bloggerbackend.domain.UserCredential;
import com.d2dsoftware.bloggerbackend.domain.utils.Authority;
import com.d2dsoftware.bloggerbackend.domain.utils.BlogPostCategory;
import com.d2dsoftware.bloggerbackend.domain.utils.Systems;
import com.d2dsoftware.bloggerbackend.dtos.BlogPostInputDto;
import com.d2dsoftware.bloggerbackend.repositories.BlogPostRepository;
import com.d2dsoftware.bloggerbackend.repositories.UserCredentialsRepository;
import com.d2dsoftware.bloggerbackend.security.TokenFactory;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BloggerBackendApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class BlogPostReaderITests {

	private static final String BLOG_POST_TITLE = "This is title";
	private static final String BLOG_POST_CONTENT = "<p>This is blog content</p>";
	private static final String BLOG_POST_DESCRIPTION = "<h3>This is blog description</h3>";
	private static final String BLOG_POST_COVER_IMAGE = "http://d2dsoftware.com/public/1.png";
	private static final String AUTHENTICATION_HEADER_NAME = "Authorization";
	private static final String BLOG_POST_ENDPOINT = "/blogger/blog_posts";
	private static final String BLOG_POST_ENDPOINT_WITH_ID = "/blogger/blog_posts/public/{blogPostId}";
	private static final String BLOG_POST_ID_PATH_PARAM_NAME = "blogPostId";
	private static final String TEST_PASSWORD = "test_password";
	private static final String TEST_USERNAME = "test_user";

	@Value("${local.server.port}")
	private int port;

	@Autowired
	private UserCredentialsRepository userCredentialsRepository;

	@Autowired
	private BlogPostRepository blogPostRepository;

	@Autowired
	private TokenFactory tokenFactory;

	private String token;

	@Before
	public void setUp() {
		RestAssured.port = port;
		userCredentialsRepository.deleteAll();
		blogPostRepository.deleteAll();

		userCredentialsRepository.save(createTestCredentials());
		token = tokenFactory.generteJWT(TEST_USERNAME);
	}

	@After
	public void cleanUp() {
		userCredentialsRepository.deleteAll();
		blogPostRepository.deleteAll();
	}

	// get blog post by Id
	@Test
	public void testGetBlogPostById() {
		String savedBlogPost = given().header(AUTHENTICATION_HEADER_NAME, token).contentType(ContentType.JSON).when()
				.content(new BlogPostInputDto(BLOG_POST_TITLE, BLOG_POST_DESCRIPTION, BLOG_POST_CONTENT,
						BLOG_POST_COVER_IMAGE, BlogPostCategory.ABOUT, Systems.MANUSHA_W_BRAND_BLOG, true))
				.post(BLOG_POST_ENDPOINT).then().statusCode(HttpStatus.SC_CREATED).extract().asString();

		final String savedBlogPostId = JsonPath.from(savedBlogPost).getString("id");
		final String savedBlogPostCreatedAt = JsonPath.from(savedBlogPost).getString("createdAt");
		final String savedBlogPostUpdatedAt = JsonPath.from(savedBlogPost).getString("updatedAt");

		given().contentType(ContentType.JSON).when().pathParameter(BLOG_POST_ID_PATH_PARAM_NAME, savedBlogPostId)
				.get(BLOG_POST_ENDPOINT_WITH_ID).then().statusCode(HttpStatus.SC_OK)
				.body("id", equalTo(savedBlogPostId)).body("title", equalTo(BLOG_POST_TITLE))
				.body("description", equalTo(BLOG_POST_DESCRIPTION)).body("content", equalTo(BLOG_POST_CONTENT))
				.body("coverImage", equalTo(BLOG_POST_COVER_IMAGE))
				.body("ownedSystem", equalTo(Systems.MANUSHA_W_BRAND_BLOG.name())).body("published", equalTo(true))
				.body("createdAt", equalTo(savedBlogPostCreatedAt)).body("updatedAt", equalTo(savedBlogPostUpdatedAt));
	}

	@Test
	public void testGetBlogPostByIdWithNonExistingPostId() {
		given().contentType(ContentType.JSON).when().pathParameter(BLOG_POST_ID_PATH_PARAM_NAME, "jhjfhjf")
				.get(BLOG_POST_ENDPOINT_WITH_ID).then().statusCode(HttpStatus.SC_NOT_FOUND);
	}

	private UserCredential createTestCredentials() {
		return new UserCredential.Builder().username(TEST_USERNAME).password(TEST_PASSWORD).enabled(true)
				.authority(Authority.BLOGGER_WRITER).build();
	}

}
