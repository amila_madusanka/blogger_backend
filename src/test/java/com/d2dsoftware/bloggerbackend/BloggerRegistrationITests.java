package com.d2dsoftware.bloggerbackend;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.notNullValue;

import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.d2dsoftware.bloggerbackend.domain.utils.Authority;
import com.d2dsoftware.bloggerbackend.dtos.UserRegisterInputDto;
import com.d2dsoftware.bloggerbackend.repositories.UserCredentialsRepository;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BloggerBackendApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class BloggerRegistrationITests {

	private static final String BLOGGER_REGISTRATION_ENDPOINT = "/blogger/authenticate/register";
	private static final String TEST_PASSWORD = "test_password";
	private static final String TEST_USERNAME = "test_user";

	@Value("${local.server.port}")
	private int port;

	@Autowired
	private UserCredentialsRepository userCredentialsRepository;

	@Before
	public void setUp() {
		RestAssured.port = port;
		userCredentialsRepository.deleteAll();
	}

	@After
	public void cleanUp() {
		userCredentialsRepository.deleteAll();
	}

	@Test
	public void testRegisterUser() {
		given().contentType(ContentType.JSON).when()
				.content(new UserRegisterInputDto(TEST_USERNAME, TEST_PASSWORD, Authority.BLOGGER_WRITER, true))
				.post(BLOGGER_REGISTRATION_ENDPOINT).then().statusCode(HttpStatus.SC_CREATED)
				.body("id", notNullValue());
	}

	@Test
	public void testRegisterUserWithNullUsername() {
		given().contentType(ContentType.JSON).when()
				.content(new UserRegisterInputDto(null, TEST_PASSWORD, Authority.BLOGGER_WRITER, true))
				.post(BLOGGER_REGISTRATION_ENDPOINT).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testRegisterUserWithEmptyUsername() {
		given().contentType(ContentType.JSON).when()
				.content(new UserRegisterInputDto("", TEST_PASSWORD, Authority.BLOGGER_WRITER, true))
				.post(BLOGGER_REGISTRATION_ENDPOINT).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testRegisterUserWithNullPassword() {
		given().contentType(ContentType.JSON).when()
				.content(new UserRegisterInputDto(TEST_USERNAME, null, Authority.BLOGGER_WRITER, true))
				.post(BLOGGER_REGISTRATION_ENDPOINT).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testRegisterUserWithEmptyPassword() {
		given().contentType(ContentType.JSON).when()
				.content(new UserRegisterInputDto(TEST_USERNAME, "", Authority.BLOGGER_WRITER, true))
				.post(BLOGGER_REGISTRATION_ENDPOINT).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testRegisterUserWithNullAuthority() {
		given().contentType(ContentType.JSON).when()
				.content(new UserRegisterInputDto(TEST_USERNAME, TEST_PASSWORD, null, true))
				.post(BLOGGER_REGISTRATION_ENDPOINT).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testRegisterUserWithNullEnabled() {
		given().contentType(ContentType.JSON).when()
				.content(new UserRegisterInputDto(TEST_USERNAME, TEST_PASSWORD, Authority.BLOGGER_WRITER, null))
				.post(BLOGGER_REGISTRATION_ENDPOINT).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

}
