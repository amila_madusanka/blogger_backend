package com.d2dsoftware.bloggerbackend;

import static com.jayway.restassured.RestAssured.given;

import java.io.File;

import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.d2dsoftware.bloggerbackend.domain.UserCredential;
import com.d2dsoftware.bloggerbackend.domain.utils.Authority;
import com.d2dsoftware.bloggerbackend.repositories.UserCredentialsRepository;
import com.d2dsoftware.bloggerbackend.security.TokenFactory;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.json.JsonPath;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BloggerBackendApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class BloggerStorageITests {

	private static final String FILE_STORAGE_PATH_PREFIX = "http://localhost:8080/blogger/storage/public/";
	private static final String SAMPLE_FILE_LOCATION = "src/test/resources/sample.jpg";
	private static final String AUTHENTICATION_HEADER_NAME = "Authorization";
	private static final String BLOGGER_STORAGE_ENDPOINT = "/blogger/storage";
	private static final String TEST_PASSWORD = "test_password";
	private static final String TEST_USERNAME = "test_user";

	@Value("${local.server.port}")
	private int port;

	@Autowired
	private UserCredentialsRepository userCredentialsRepository;

	@Autowired
	private TokenFactory tokenFactory;

	@Autowired
	private GridFsTemplate gridFsTemplate;

	private String token;

	@Before
	public void setUp() {
		RestAssured.port = port;
		userCredentialsRepository.deleteAll();
		gridFsTemplate.delete(null);

		userCredentialsRepository.save(createTestCredentials());
		token = tokenFactory.generteJWT(TEST_USERNAME);
	}

	@After
	public void cleanUp() {
		userCredentialsRepository.deleteAll();
		gridFsTemplate.delete(null);
	}

	@Test
	public void testStoreFile() {
		String result = given().header(AUTHENTICATION_HEADER_NAME, token).when().contentType("multipart/form-data")
				.multiPart("file", new File(SAMPLE_FILE_LOCATION)).post(BLOGGER_STORAGE_ENDPOINT).then()
				.statusCode(HttpStatus.SC_OK).extract().asString();

		Assert.assertNotNull(result);
		Assert.assertTrue(JsonPath.from(result).getString("location").startsWith(FILE_STORAGE_PATH_PREFIX));

	}

	private UserCredential createTestCredentials() {
		return new UserCredential.Builder().username(TEST_USERNAME).password(TEST_PASSWORD).enabled(true)
				.authority(Authority.BLOGGER_WRITER).build();
	}

}
