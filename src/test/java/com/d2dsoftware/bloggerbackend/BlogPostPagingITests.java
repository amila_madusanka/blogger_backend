package com.d2dsoftware.bloggerbackend;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.nullValue;

import java.util.stream.IntStream;

import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.d2dsoftware.bloggerbackend.domain.UserCredential;
import com.d2dsoftware.bloggerbackend.domain.utils.Authority;
import com.d2dsoftware.bloggerbackend.domain.utils.BlogPostCategory;
import com.d2dsoftware.bloggerbackend.domain.utils.Systems;
import com.d2dsoftware.bloggerbackend.dtos.BlogPostInputDto;
import com.d2dsoftware.bloggerbackend.repositories.BlogPostRepository;
import com.d2dsoftware.bloggerbackend.repositories.UserCredentialsRepository;
import com.d2dsoftware.bloggerbackend.security.TokenFactory;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BloggerBackendApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class BlogPostPagingITests {

	private static final String BLOG_POST_TITLE = "This is title";
	private static final String BLOG_POST_CONTENT = "<p>This is blog content</p>";
	private static final String BLOG_POST_DESCRIPTION = "<h3>This is blog description</h3>";
	private static final String BLOG_POST_COVER_IMAGE = "http://d2dsoftware.com/public/1.png";
	private static final String AUTHENTICATION_HEADER_NAME = "Authorization";
	private static final String NEW_BLOG_POST_ENDPOINT = "/blogger/blog_posts";
	private static final String BLOG_POST_MANAGEMENT_ENDPOINT = "/blogger/blog_posts/{system}/management";
	private static final String BLOG_POST_SYSTEM_PATH_PARAM_NAME = "system";
	private static final String PAGE_QUERY_PARAM_NAME = "page";
	private static final String SIZE_QUERY_PARAM_NAME = "size";
	private static final String SORT_QUERY_PARAM_NAME = "sort";
	private static final String TEST_PASSWORD = "test_password";
	private static final String TEST_USERNAME = "test_user";

	@Value("${local.server.port}")
	private int port;

	@Autowired
	private UserCredentialsRepository userCredentialsRepository;

	@Autowired
	private BlogPostRepository blogPostRepository;

	@Autowired
	private TokenFactory tokenFactory;

	private String token;

	@Before
	public void setUp() {
		RestAssured.port = port;
		userCredentialsRepository.deleteAll();
		blogPostRepository.deleteAll();

		userCredentialsRepository.save(createTestCredentials());
		token = tokenFactory.generteJWT(TEST_USERNAME);

		createTestBlogPostData();
	}

	@After
	public void cleanUp() {
		userCredentialsRepository.deleteAll();
		blogPostRepository.deleteAll();
	}

	// get blog posts for management
	@Test
	public void testGetAllBlogPostsForManagementWithPageAndSize() {
		// get first page
		given().header(AUTHENTICATION_HEADER_NAME, token).contentType(ContentType.JSON).when()
				.pathParam(BLOG_POST_SYSTEM_PATH_PARAM_NAME, Systems.MANUSHA_W_BRAND_BLOG.name())
				.queryParam(PAGE_QUERY_PARAM_NAME, 0).queryParam(SIZE_QUERY_PARAM_NAME, 3)
				.get(BLOG_POST_MANAGEMENT_ENDPOINT).then().statusCode(HttpStatus.SC_OK)
				.body("content.size()", equalTo(3)).body("totalElements", equalTo(5)).body("totalPages", equalTo(2))
				.body("last", equalTo(false)).body("size", equalTo(3)).body("number", equalTo(0))
				.body("first", equalTo(true)).body("sort", nullValue()).body("numberOfElements", equalTo(3))
				.body("content.title",
						hasItems(BLOG_POST_TITLE + " 1", BLOG_POST_TITLE + " 2", BLOG_POST_TITLE + " 3"));

		// get second page
		given().header(AUTHENTICATION_HEADER_NAME, token).contentType(ContentType.JSON).when()
				.pathParam(BLOG_POST_SYSTEM_PATH_PARAM_NAME, Systems.MANUSHA_W_BRAND_BLOG.name())
				.queryParam(PAGE_QUERY_PARAM_NAME, 1).queryParam(SIZE_QUERY_PARAM_NAME, 3)
				.get(BLOG_POST_MANAGEMENT_ENDPOINT).then().statusCode(HttpStatus.SC_OK)
				.body("content.size()", equalTo(2)).body("totalElements", equalTo(5)).body("totalPages", equalTo(2))
				.body("last", equalTo(true)).body("size", equalTo(3)).body("number", equalTo(1))
				.body("first", equalTo(false)).body("sort", nullValue()).body("numberOfElements", equalTo(2))
				.body("content.title", hasItems(BLOG_POST_TITLE + " 4", BLOG_POST_TITLE + " 5"));
	}

	@Test
	public void testGetAllBlogPostsForManagementWithSort() {
		// get first page
		given().header(AUTHENTICATION_HEADER_NAME, token).contentType(ContentType.JSON).when()
				.pathParam(BLOG_POST_SYSTEM_PATH_PARAM_NAME, Systems.MANUSHA_W_BRAND_BLOG.name())
				.queryParam(PAGE_QUERY_PARAM_NAME, 0).queryParam(SIZE_QUERY_PARAM_NAME, 3)
				.queryParam(SORT_QUERY_PARAM_NAME, "createdAt,DESC").get(BLOG_POST_MANAGEMENT_ENDPOINT).then()
				.statusCode(HttpStatus.SC_OK).body("content.size()", equalTo(3)).body("totalElements", equalTo(5))
				.body("totalPages", equalTo(2)).body("last", equalTo(false)).body("size", equalTo(3))
				.body("number", equalTo(0)).body("first", equalTo(true)).body("sort[0].direction", equalTo("DESC"))
				.body("sort[0].property", equalTo("createdAt")).body("sort[0].ascending", equalTo(false))
				.body("numberOfElements", equalTo(3)).body("content.title",
						hasItems(BLOG_POST_TITLE + " 5", BLOG_POST_TITLE + " 4", BLOG_POST_TITLE + " 3"));

		// get second page
		given().header(AUTHENTICATION_HEADER_NAME, token).contentType(ContentType.JSON).when()
				.pathParam(BLOG_POST_SYSTEM_PATH_PARAM_NAME, Systems.MANUSHA_W_BRAND_BLOG.name())
				.queryParam(PAGE_QUERY_PARAM_NAME, 1).queryParam(SIZE_QUERY_PARAM_NAME, 3)
				.queryParam(SORT_QUERY_PARAM_NAME, "createdAt,DESC").get(BLOG_POST_MANAGEMENT_ENDPOINT).then()
				.statusCode(HttpStatus.SC_OK).body("content.size()", equalTo(2)).body("totalElements", equalTo(5))
				.body("totalPages", equalTo(2)).body("last", equalTo(true)).body("size", equalTo(3))
				.body("number", equalTo(1)).body("first", equalTo(false)).body("sort[0].direction", equalTo("DESC"))
				.body("sort[0].property", equalTo("createdAt")).body("sort[0].ascending", equalTo(false))
				.body("numberOfElements", equalTo(2))
				.body("content.title", hasItems(BLOG_POST_TITLE + " 2", BLOG_POST_TITLE + " 1"));
	}

	private void createTestBlogPostData() {
		IntStream.range(0, 5).forEach(i -> {
			given().header(AUTHENTICATION_HEADER_NAME, token).contentType(ContentType.JSON).when()
					.content(new BlogPostInputDto(BLOG_POST_TITLE + " " + (i + 1), BLOG_POST_DESCRIPTION,
							BLOG_POST_CONTENT, BLOG_POST_COVER_IMAGE, BlogPostCategory.ABOUT,
							Systems.MANUSHA_W_BRAND_BLOG, true))
					.post(NEW_BLOG_POST_ENDPOINT).then().statusCode(HttpStatus.SC_CREATED);
		});
	}

	private UserCredential createTestCredentials() {
		return new UserCredential.Builder().username(TEST_USERNAME).password(TEST_PASSWORD).enabled(true)
				.authority(Authority.BLOGGER_WRITER).build();
	}

}
