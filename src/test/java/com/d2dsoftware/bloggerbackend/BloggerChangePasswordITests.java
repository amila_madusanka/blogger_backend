package com.d2dsoftware.bloggerbackend;

import static com.jayway.restassured.RestAssured.given;

import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.d2dsoftware.bloggerbackend.domain.UserCredential;
import com.d2dsoftware.bloggerbackend.domain.utils.Authority;
import com.d2dsoftware.bloggerbackend.dtos.ChangePasswordInputDto;
import com.d2dsoftware.bloggerbackend.repositories.UserCredentialsRepository;
import com.d2dsoftware.bloggerbackend.security.TokenFactory;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BloggerBackendApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class BloggerChangePasswordITests {

	private static final String AUTHENTICATION_HEADER_NAME = "Authorization";
	private static final String BLOGGER_CHANGE_PASSWORD_ENDPOINT = "/blogger/authenticate/cpwd";
	private static final String TEST_PASSWORD = "test_password";
	private static final String NEW_PASSWORD = "new_password";
	private static final String TEST_USERNAME = "test_user";

	@Value("${local.server.port}")
	private int port;

	@Autowired
	private UserCredentialsRepository userCredentialsRepository;

	@Autowired
	private TokenFactory tokenFactory;

	private String token;
	private UserCredential userCredential;

	@Before
	public void setUp() {
		RestAssured.port = port;
		userCredentialsRepository.deleteAll();

		userCredential = userCredentialsRepository.save(createTestCredentials());
		token = tokenFactory.generteJWT(TEST_USERNAME);
	}

	@After
	public void cleanUp() {
		userCredentialsRepository.deleteAll();
	}

	@Test
	public void testChangePassword() {
		given().header(AUTHENTICATION_HEADER_NAME, token).contentType(ContentType.JSON).when()
				.content(new ChangePasswordInputDto(TEST_PASSWORD, NEW_PASSWORD)).put(BLOGGER_CHANGE_PASSWORD_ENDPOINT)
				.then().statusCode(HttpStatus.SC_OK);

		UserCredential userCredentialAfterChangePassword = userCredentialsRepository.findOne(userCredential.getId());

		Assert.assertEquals(1, userCredentialsRepository.count());
		Assert.assertEquals(NEW_PASSWORD, userCredentialAfterChangePassword.getPassword());
		Assert.assertEquals(userCredential.getCreatedAt().toString(),
				userCredentialAfterChangePassword.getCreatedAt().toString());
		Assert.assertNotEquals(userCredential.getUpdatedAt(), userCredentialAfterChangePassword.getUpdatedAt());

	}

	@Test
	public void testChangePasswordWithNullOldPassword() {
		given().header(AUTHENTICATION_HEADER_NAME, token).contentType(ContentType.JSON).when()
				.content(new ChangePasswordInputDto(null, NEW_PASSWORD)).put(BLOGGER_CHANGE_PASSWORD_ENDPOINT).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testChangePasswordWithEmptyOldPassword() {
		given().header(AUTHENTICATION_HEADER_NAME, token).contentType(ContentType.JSON).when()
				.content(new ChangePasswordInputDto("", NEW_PASSWORD)).put(BLOGGER_CHANGE_PASSWORD_ENDPOINT).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testChangePasswordWithNullNewPassword() {
		given().header(AUTHENTICATION_HEADER_NAME, token).contentType(ContentType.JSON).when()
				.content(new ChangePasswordInputDto(TEST_PASSWORD, null)).put(BLOGGER_CHANGE_PASSWORD_ENDPOINT).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testChangePasswordWithEmptyNewPassword() {
		given().header(AUTHENTICATION_HEADER_NAME, token).contentType(ContentType.JSON).when()
				.content(new ChangePasswordInputDto(TEST_PASSWORD, "")).put(BLOGGER_CHANGE_PASSWORD_ENDPOINT).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testChangePasswordWithInvalidToken() {
		given().header(AUTHENTICATION_HEADER_NAME, "abc").contentType(ContentType.JSON).when()
				.content(new ChangePasswordInputDto(TEST_PASSWORD, NEW_PASSWORD)).put(BLOGGER_CHANGE_PASSWORD_ENDPOINT)
				.then().statusCode(HttpStatus.SC_FORBIDDEN);

	}

	@Test
	public void testChangePasswordWithWrongOldPassword() {
		given().header(AUTHENTICATION_HEADER_NAME, token).contentType(ContentType.JSON).when()
				.content(new ChangePasswordInputDto(NEW_PASSWORD, NEW_PASSWORD)).put(BLOGGER_CHANGE_PASSWORD_ENDPOINT)
				.then().statusCode(HttpStatus.SC_NOT_FOUND);

	}

	private UserCredential createTestCredentials() {
		return new UserCredential.Builder().username(TEST_USERNAME).password(TEST_PASSWORD).enabled(true)
				.authority(Authority.BLOGGER_WRITER).build();
	}

}
