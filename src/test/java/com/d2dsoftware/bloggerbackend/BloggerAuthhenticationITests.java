package com.d2dsoftware.bloggerbackend;

import static com.jayway.restassured.RestAssured.given;

import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.d2dsoftware.bloggerbackend.domain.UserCredential;
import com.d2dsoftware.bloggerbackend.domain.utils.Authority;
import com.d2dsoftware.bloggerbackend.dtos.AuthenticationInputDto;
import com.d2dsoftware.bloggerbackend.repositories.UserCredentialsRepository;
import com.d2dsoftware.bloggerbackend.security.TokenFactory;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BloggerBackendApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class BloggerAuthhenticationITests {

	private static final String BLOGGER_AUTHENTICATION_ENDPOINT = "/blogger/authenticate";
	private static final String TEST_PASSWORD = "test_password";
	private static final String TEST_USERNAME = "test_user";

	@Value("${local.server.port}")
	private int port;

	@Autowired
	private UserCredentialsRepository userCredentialsRepository;

	@Autowired
	private TokenFactory tokenFactory;

	private UserCredential credential;

	@Before
	public void setUp() {
		RestAssured.port = port;
		userCredentialsRepository.deleteAll();

		credential = userCredentialsRepository.save(createTestCredentials());
	}

	@After
	public void cleanUp() {
		userCredentialsRepository.deleteAll();
	}

	@Test
	public void testAuthenticationWithCorrectAuthInfo() {

		String output = given().contentType(ContentType.JSON).when()
				.content(new AuthenticationInputDto(TEST_USERNAME, TEST_PASSWORD)).post(BLOGGER_AUTHENTICATION_ENDPOINT)
				.then().statusCode(HttpStatus.SC_OK).extract().asString();
		Assert.assertEquals(output, tokenFactory.generteOutputToken(credential));

	}

	@Test
	public void testAuthenticationWithInCorrectPassword() {

		given().contentType(ContentType.JSON).when().content(new AuthenticationInputDto(TEST_USERNAME, "abc"))
				.post(BLOGGER_AUTHENTICATION_ENDPOINT).then().statusCode(HttpStatus.SC_UNAUTHORIZED);

	}

	@Test
	public void testAuthenticationWithNullPassword() {

		given().contentType(ContentType.JSON).when().content(new AuthenticationInputDto(TEST_USERNAME, null))
				.post(BLOGGER_AUTHENTICATION_ENDPOINT).then().statusCode(HttpStatus.SC_BAD_REQUEST);

	}

	@Test
	public void testAuthenticationWithEmptyPassword() {

		given().contentType(ContentType.JSON).when().content(new AuthenticationInputDto(TEST_USERNAME, ""))
				.post(BLOGGER_AUTHENTICATION_ENDPOINT).then().statusCode(HttpStatus.SC_BAD_REQUEST);

	}

	@Test
	public void testAuthenticationWithInCorrectUsername() {

		given().contentType(ContentType.JSON).when().content(new AuthenticationInputDto("abc", TEST_PASSWORD))
				.post(BLOGGER_AUTHENTICATION_ENDPOINT).then().statusCode(HttpStatus.SC_UNAUTHORIZED);

	}

	@Test
	public void testAuthenticationWithNullUsername() {

		given().contentType(ContentType.JSON).when().content(new AuthenticationInputDto(null, TEST_PASSWORD))
				.post(BLOGGER_AUTHENTICATION_ENDPOINT).then().statusCode(HttpStatus.SC_BAD_REQUEST);

	}

	@Test
	public void testAuthenticationWithEmptyUsername() {

		given().contentType(ContentType.JSON).when().content(new AuthenticationInputDto("", TEST_PASSWORD))
				.post(BLOGGER_AUTHENTICATION_ENDPOINT).then().statusCode(HttpStatus.SC_BAD_REQUEST);

	}

	private UserCredential createTestCredentials() {
		return new UserCredential.Builder().username(TEST_USERNAME).password(TEST_PASSWORD).enabled(true)
				.authority(Authority.BLOGGER_WRITER).build();
	}

}
