package com.d2dsoftware.bloggerbackend.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BlogPostReaderDto {
	private final String id;
	private final String title;
	private final String coverImage;
	private final String description;
	private final String createdAt;

	@JsonCreator
	public BlogPostReaderDto(@JsonProperty("id") String id, @JsonProperty("title") String title,
			@JsonProperty("coverImage") String coverImage, @JsonProperty("description") String description,
			@JsonProperty("createdAt") String createdAt) {
		super();
		this.id = id;
		this.title = title;
		this.coverImage = coverImage;
		this.description = description;
		this.createdAt = createdAt;
	}

	public String getTitle() {
		return this.title;
	}

	public String getId() {
		return this.id;
	}

	public String getCreatedAt() {
		return this.createdAt;
	}

	public String getDescription() {
		return this.description;
	}

	public String getCoverImage() {
		return this.coverImage;
	}
}
