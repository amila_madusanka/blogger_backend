package com.d2dsoftware.bloggerbackend.dtos;

import com.d2dsoftware.bloggerbackend.domain.utils.BlogPostCategory;
import com.d2dsoftware.bloggerbackend.domain.utils.Systems;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BlogPostOutputDto {
	private final String id;
	private final String title;
	private final String description;
	private final String content;
	private final String coverImage;
	private final BlogPostCategory category;
	private final Systems ownedSystem;
	private final boolean published;
	private final String createdAt;
	private final String updatedAt;

	@JsonCreator
	public BlogPostOutputDto(@JsonProperty("id") String id, @JsonProperty("title") String title,
			@JsonProperty("description") String description, @JsonProperty("content") String content,
			@JsonProperty("coverImage") String coverImage, @JsonProperty("category") BlogPostCategory category,
			@JsonProperty("ownedSystem") Systems ownedSystem, @JsonProperty("published") boolean published,
			@JsonProperty("createdAt") String createdAt, @JsonProperty("updatedAt") String updatedAt) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.content = content;
		this.coverImage = coverImage;
		this.category = category;
		this.ownedSystem = ownedSystem;
		this.published = published;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public String getTitle() {
		return this.title;
	}

	public String getId() {
		return this.id;
	}

	public String getDescription() {
		return this.description;
	}

	public BlogPostCategory getCategory() {
		return this.category;
	}

	public String getContent() {
		return this.content;
	}

	public Systems getOwnedSystem() {
		return this.ownedSystem;
	}

	public boolean isPublished() {
		return this.published;
	}

	public String getCreatedAt() {
		return this.createdAt;
	}

	public String getUpdatedAt() {
		return this.updatedAt;
	}

	public String getCoverImage() {
		return coverImage;
	}

}
