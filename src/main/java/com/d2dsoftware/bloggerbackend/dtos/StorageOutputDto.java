package com.d2dsoftware.bloggerbackend.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StorageOutputDto {
	private final String location;

	@JsonCreator
	public StorageOutputDto(@JsonProperty("location") String location) {
		super();
		this.location = location;
	}

	public String getLocation() {
		return location;
	}

}
