package com.d2dsoftware.bloggerbackend.dtos;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.d2dsoftware.bloggerbackend.domain.utils.Authority;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserRegisterInputDto {

	@NotNull(message = "'username' cannot be null")
	@NotEmpty(message = "'username' cannot be empty")
	private final String username;

	@NotNull(message = "'password' cannot be null")
	@NotEmpty(message = "'password' cannot be empty")
	private final String password;

	@NotNull(message = "'authority' cannot be null")
	private final Authority authority;

	@NotNull(message = "'enabled' cannot be null")
	private final Boolean enabled;

	@JsonCreator
	public UserRegisterInputDto(@JsonProperty("username") String username, @JsonProperty("password") String password,
			@JsonProperty("authority") Authority authority, @JsonProperty("enabled") Boolean enabled) {
		super();
		this.username = username;
		this.password = password;
		this.authority = authority;
		this.enabled = enabled;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

	public Authority getAuthority() {
		return this.authority;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

}
