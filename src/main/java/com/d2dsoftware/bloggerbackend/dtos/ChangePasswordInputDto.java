package com.d2dsoftware.bloggerbackend.dtos;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ChangePasswordInputDto {
	@NotNull(message = "'oldPassword' cannot be null")
	@NotEmpty(message = "'oldPassword' cannot be empty")
	private final String oldPassword;

	@NotNull(message = "'newPassword' cannot be null")
	@NotEmpty(message = "'newPassword' cannot be empty")
	private final String newPassword;

	@JsonCreator
	public ChangePasswordInputDto(@JsonProperty("oldPassword") String oldPassword,
			@JsonProperty("newPassword") String newPassword) {
		super();
		this.oldPassword = oldPassword;
		this.newPassword = newPassword;
	}

	public String getOldPassword() {
		return this.oldPassword;
	}

	public String getNewPassword() {
		return this.newPassword;
	}

}
