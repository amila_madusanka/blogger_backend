package com.d2dsoftware.bloggerbackend.dtos;

import com.d2dsoftware.bloggerbackend.domain.utils.BlogPostCategory;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BlogPostManagementDto {
	private final String id;
	private final String title;
	private final String coverImage;
	private final BlogPostCategory category;
	private final boolean published;
	private final String createdAt;
	private final String updatedAt;

	@JsonCreator
	public BlogPostManagementDto(@JsonProperty("id") String id, @JsonProperty("title") String title,
			@JsonProperty("coverImage") String coverImage, @JsonProperty("category") BlogPostCategory category,
			@JsonProperty("published") boolean published, @JsonProperty("createdAt") String createdAt,
			@JsonProperty("updatedAt") String updatedAt) {
		super();
		this.id = id;
		this.title = title;
		this.coverImage = coverImage;
		this.category = category;
		this.published = published;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public String getTitle() {
		return this.title;
	}

	public String getId() {
		return this.id;
	}

	public BlogPostCategory getCategory() {
		return this.category;
	}

	public boolean isPublished() {
		return this.published;
	}

	public String getCreatedAt() {
		return this.createdAt;
	}

	public String getUpdatedAt() {
		return this.updatedAt;
	}

	public String getCoverImage() {
		return this.coverImage;
	}
}
