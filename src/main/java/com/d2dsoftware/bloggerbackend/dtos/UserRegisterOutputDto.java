package com.d2dsoftware.bloggerbackend.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserRegisterOutputDto {
	private final String id;

	@JsonCreator
	public UserRegisterOutputDto(@JsonProperty("id") String id) {
		super();
		this.id = id;
	}

	public String getId() {
		return this.id;
	}
}
