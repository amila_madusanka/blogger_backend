package com.d2dsoftware.bloggerbackend.dtos;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthenticationInputDto {
	@NotNull(message = "'username' cannot be null")
	@NotEmpty(message = "'username' cannot be empty")
	private final String username;

	@NotNull(message = "'password' cannot be null")
	@NotEmpty(message = "'password' cannot be empty")
	private final String password;

	@JsonCreator
	public AuthenticationInputDto(@JsonProperty("username") String username,
			@JsonProperty("password") String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}
}
