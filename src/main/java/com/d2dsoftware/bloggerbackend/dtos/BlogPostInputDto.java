package com.d2dsoftware.bloggerbackend.dtos;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.d2dsoftware.bloggerbackend.domain.utils.BlogPostCategory;
import com.d2dsoftware.bloggerbackend.domain.utils.Systems;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BlogPostInputDto {

	@NotNull(message = "'title' cannot be null")
	@NotEmpty(message = "'title' cannot be empty")
	private final String title;

	@NotNull(message = "'description' cannot be null")
	@NotEmpty(message = "'description' cannot be empty")
	private final String description;

	@NotNull(message = "'content' cannot be null")
	@NotEmpty(message = "'content' cannot be empty")
	private final String content;

	@NotNull(message = "'coverImage' cannot be null")
	@NotEmpty(message = "'coverImage' cannot be empty")
	private final String coverImage;

	@NotNull(message = "'category' cannot be null")
	private final BlogPostCategory category;

	@NotNull(message = "'ownedSystem' cannot be null")
	private final Systems ownedSystem;

	@NotNull(message = "'published' cannot be null")
	private final Boolean published;

	@JsonCreator
	public BlogPostInputDto(@JsonProperty("title") String title, @JsonProperty("description") String description,
			@JsonProperty("content") String content, @JsonProperty("coverImage") String coverImage,
			@JsonProperty("category") BlogPostCategory category, @JsonProperty("ownedSystem") Systems ownedSystem,
			@JsonProperty("published") Boolean published) {
		super();
		this.title = title;
		this.description = description;
		this.content = content;
		this.coverImage = coverImage;
		this.ownedSystem = ownedSystem;
		this.published = published;
		this.category = category;
	}

	public String getTitle() {
		return this.title;
	}

	public String getDescription() {
		return this.description;
	}

	public BlogPostCategory getCategory() {
		return this.category;
	}

	public String getContent() {
		return this.content;
	}

	public String getCoverImage() {
		return this.coverImage;
	}

	public Systems getOwnedSystem() {
		return this.ownedSystem;
	}

	public Boolean isPublished() {
		return this.published;
	}

}
