package com.d2dsoftware.bloggerbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@SpringBootApplication
@EnableMongoAuditing
public class BloggerBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BloggerBackendApplication.class, args);
	}
}
