package com.d2dsoftware.bloggerbackend.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.d2dsoftware.bloggerbackend.domain.UserCredential;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenFactory {

	@Value("${blogger.authenticate-jwt.secret}")
	private String secret;

	@Value("${blogger.authenticate-jwt.appender}")
	private String appender;

	public String generteOutputToken(UserCredential credential) {
		final String tokenBody = credential.getId() + appender + credential.getUsername() + appender
				+ credential.getAuthority().name() + appender + this.generteJWT(credential.getUsername());
		return Jwts.builder().setSubject(tokenBody).signWith(SignatureAlgorithm.HS512, secret).compact();
	}

	public String generteJWT(String username) {
		return Jwts.builder().setSubject(username).signWith(SignatureAlgorithm.HS512, secret).compact();
	}

	public String parseToken(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
	}
}
