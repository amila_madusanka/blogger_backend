package com.d2dsoftware.bloggerbackend.security;

import java.util.Collection;
import java.util.LinkedList;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.d2dsoftware.bloggerbackend.domain.UserCredential;

public class AuthenticatedUser implements Authentication {

	private static final long serialVersionUID = 1L;
	private final UserCredential credential;
	private boolean authenticated = true;

	public AuthenticatedUser(final UserCredential credential) {
		this.credential = credential;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(this.credential.getAuthority().name());
		Collection<GrantedAuthority> grantedAuthorities = new LinkedList<GrantedAuthority>();
		grantedAuthorities.add(grantedAuthority);
		return grantedAuthorities;
	}

	@Override
	public Object getCredentials() {
		return this.credential.getPassword();
	}

	@Override
	public Object getDetails() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return this.credential.getUsername();
	}

	@Override
	public boolean isAuthenticated() {
		return this.authenticated;
	}

	@Override
	public void setAuthenticated(boolean b) {
		this.authenticated = b;
	}

	@Override
	public String getName() {
		return this.credential.getUsername();
	}
}
