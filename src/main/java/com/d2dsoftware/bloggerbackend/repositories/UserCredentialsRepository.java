package com.d2dsoftware.bloggerbackend.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.d2dsoftware.bloggerbackend.domain.UserCredential;

public interface UserCredentialsRepository extends MongoRepository<UserCredential, String> {

	UserCredential findByUsername(String username);

	UserCredential findByUsernameAndPassword(String username, String password);
}
