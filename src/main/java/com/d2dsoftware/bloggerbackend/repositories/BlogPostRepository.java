package com.d2dsoftware.bloggerbackend.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.d2dsoftware.bloggerbackend.domain.BlogPost;
import com.d2dsoftware.bloggerbackend.domain.utils.BlogPostCategory;
import com.d2dsoftware.bloggerbackend.domain.utils.Systems;

public interface BlogPostRepository extends PagingAndSortingRepository<BlogPost, String> {
	Page<BlogPost> findByOwnedSystem(Systems ownedSystem, Pageable pageable);

	Page<BlogPost> findByOwnedSystemAndPublished(Systems ownedSystem, boolean published, Pageable pageable);

	Page<BlogPost> findByOwnedSystemAndCategoryAndPublished(Systems ownedSystem, BlogPostCategory category,
			boolean published, Pageable pageable);
}
