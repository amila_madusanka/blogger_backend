package com.d2dsoftware.bloggerbackend.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.d2dsoftware.bloggerbackend.security.ApiAuthenticationFilter;
import com.d2dsoftware.bloggerbackend.services.TokenAuthenticationService;
import com.d2dsoftware.bloggerbackend.services.impl.SecureUserDetailsService;

@Configuration
public class ApiSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private TokenAuthenticationService tokenAuthenticationService;

	@Autowired
	SecureUserDetailsService secureUserDetailsService;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.headers().cacheControl();
		http.csrf().disable().authorizeRequests().antMatchers("/health", "/info").permitAll()
				.antMatchers(HttpMethod.POST, "/blogger/authenticate/register").permitAll()
				.antMatchers(HttpMethod.OPTIONS, "/**").permitAll().antMatchers("/blogger/authenticate").permitAll()
				.antMatchers("/blogger/storage/**").permitAll().antMatchers("/blogger/blog_posts/public/**").permitAll()
				.anyRequest().authenticated().and()
				.addFilterBefore(new ApiAuthenticationFilter(tokenAuthenticationService),
						UsernamePasswordAuthenticationFilter.class);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(secureUserDetailsService);
	}

}
