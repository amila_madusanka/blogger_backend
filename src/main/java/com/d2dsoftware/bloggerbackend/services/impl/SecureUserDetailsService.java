package com.d2dsoftware.bloggerbackend.services.impl;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.d2dsoftware.bloggerbackend.domain.UserCredential;
import com.d2dsoftware.bloggerbackend.security.SecureUserDetails;
import com.d2dsoftware.bloggerbackend.services.UserCredentialService;

@Service
public class SecureUserDetailsService implements UserDetailsService {

	private final UserCredentialService userCredentialService;

	@Autowired
	public SecureUserDetailsService(final UserCredentialService userCredentialService) {
		super();
		this.userCredentialService = userCredentialService;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserCredential userCredential = this.userCredentialService.getByUsername(username);
		if (Objects.isNull(userCredential)) {
			throw new UsernameNotFoundException(username);
		}
		return new SecureUserDetails(userCredential);
	}

}
