package com.d2dsoftware.bloggerbackend.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.d2dsoftware.bloggerbackend.domain.BlogPost;
import com.d2dsoftware.bloggerbackend.domain.utils.BlogPostCategory;
import com.d2dsoftware.bloggerbackend.domain.utils.Systems;
import com.d2dsoftware.bloggerbackend.repositories.BlogPostRepository;
import com.d2dsoftware.bloggerbackend.services.BlogPostService;

@Service
public class BlogPostServiceImpl implements BlogPostService {

	private final BlogPostRepository blogPostRepository;

	@Autowired
	public BlogPostServiceImpl(final BlogPostRepository blogPostRepository) {
		super();
		this.blogPostRepository = blogPostRepository;
	}

	@Override
	public BlogPost save(BlogPost blogPost) {
		return this.blogPostRepository.save(blogPost);
	}

	@Override
	public BlogPost getById(String id) {
		return this.blogPostRepository.findOne(id);
	}

	@Override
	public void deleteById(String id) {
		this.blogPostRepository.delete(id);
	}

	@Override
	public Page<BlogPost> getBySystem(Systems ownedSystem, Pageable pageable) {
		return this.blogPostRepository.findByOwnedSystem(ownedSystem, pageable);
	}

	@Override
	public Page<BlogPost> getAllPublishedBySystem(Systems ownedSystem, Pageable pageable) {
		return this.blogPostRepository.findByOwnedSystemAndPublished(ownedSystem, true, pageable);
	}

	@Override
	public Page<BlogPost> getAllPublishedBySystemAndCategory(Systems ownedSystem, BlogPostCategory category,
			Pageable pageable) {
		return this.blogPostRepository.findByOwnedSystemAndCategoryAndPublished(ownedSystem, category, true, pageable);
	}

}
