package com.d2dsoftware.bloggerbackend.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.d2dsoftware.bloggerbackend.domain.UserCredential;
import com.d2dsoftware.bloggerbackend.repositories.UserCredentialsRepository;
import com.d2dsoftware.bloggerbackend.services.UserCredentialService;

@Service
public class UserCredentialServiceImpl implements UserCredentialService {

	private final UserCredentialsRepository userCredentialsRepository;

	@Autowired
	public UserCredentialServiceImpl(final UserCredentialsRepository userCredentialsRepository) {
		super();
		this.userCredentialsRepository = userCredentialsRepository;
	}

	@Override
	public UserCredential getByUsername(String username) {
		return this.userCredentialsRepository.findByUsername(username);
	}

	@Override
	public UserCredential getByUsernameAndPassword(String username, String password) {
		return this.userCredentialsRepository.findByUsernameAndPassword(username, password);
	}

	@Override
	public UserCredential save(UserCredential userCredential) {
		return this.userCredentialsRepository.save(userCredential);
	}

	@Override
	public UserCredential getById(String id) {
		return this.userCredentialsRepository.findOne(id);
	}

}
