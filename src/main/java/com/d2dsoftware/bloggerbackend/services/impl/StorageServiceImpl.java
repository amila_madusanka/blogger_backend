package com.d2dsoftware.bloggerbackend.services.impl;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsCriteria;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.d2dsoftware.bloggerbackend.services.StorageService;
import com.mongodb.gridfs.GridFSDBFile;

@Service
public class StorageServiceImpl implements StorageService {

	private final GridFsTemplate gridFsTemplate;

	@Autowired
	public StorageServiceImpl(final GridFsTemplate gridFsTemplate) {
		super();
		this.gridFsTemplate = gridFsTemplate;
	}

	@Override
	public String storeFile(MultipartFile file) {
		String name = prepareFileName(file.getOriginalFilename());
		try {
			this.gridFsTemplate.store(file.getInputStream(), name, file.getContentType()).save();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return name;
	}

	@Override
	public GridFSDBFile getStoredFileByName(String fileName) {
		return this.maybeLoadFile(fileName).orElse(null);
	}

	private Optional<GridFSDBFile> maybeLoadFile(String name) {
		GridFSDBFile file = this.gridFsTemplate.findOne(getFilenameQuery(name));
		return Optional.ofNullable(file);
	}

	private static Query getFilenameQuery(String name) {
		return Query.query(GridFsCriteria.whereFilename().is(name));
	}

	private String prepareFileName(String originalFileName) {
		String preparedFileName = String.valueOf(UUID.randomUUID());

		if (originalFileName.contains(".")) {
			preparedFileName += "."
					+ originalFileName.substring(originalFileName.indexOf(".") + 1, originalFileName.length());
		}

		return preparedFileName;
	}

}
