package com.d2dsoftware.bloggerbackend.services.impl;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.d2dsoftware.bloggerbackend.domain.UserCredential;
import com.d2dsoftware.bloggerbackend.security.AuthenticatedUser;
import com.d2dsoftware.bloggerbackend.security.TokenFactory;
import com.d2dsoftware.bloggerbackend.services.TokenAuthenticationService;
import com.d2dsoftware.bloggerbackend.services.UserCredentialService;

@Service
public class TokenAuthenticationServiceImpl implements TokenAuthenticationService {
	@Value("${jwt.token.prefix}")
	private String tokenPrefix;

	@Value("${jwt.header}")
	private String headerString;

	private final UserCredentialService userCredentialService;

	private final TokenFactory tokenFactory;

	@Autowired
	public TokenAuthenticationServiceImpl(final UserCredentialService userCredentialService,
			final TokenFactory tokenFactory) {
		super();
		this.userCredentialService = userCredentialService;
		this.tokenFactory = tokenFactory;
	}

	@Override
	public String getAuthenticationToken(String username) {
		return this.tokenFactory.generteJWT(username);
	}

	@Override
	public void addAuthentication(HttpServletResponse response, String username) {
		response.addHeader(headerString, tokenPrefix + " " + this.getAuthenticationToken(username));
	}

	@Override
	public Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(headerString);
		if (Objects.nonNull(token)) {
			try {
				UserCredential credential = this.userCredentialService
						.getByUsername(this.tokenFactory.parseToken(token));
				if (Objects.nonNull(credential)) {
					return new AuthenticatedUser(credential);
				}

			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}

}
