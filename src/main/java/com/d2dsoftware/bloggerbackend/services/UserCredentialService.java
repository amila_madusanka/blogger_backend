package com.d2dsoftware.bloggerbackend.services;

import com.d2dsoftware.bloggerbackend.domain.UserCredential;

public interface UserCredentialService {
	UserCredential getByUsername(String username);

	UserCredential getById(String id);

	UserCredential getByUsernameAndPassword(String username, String password);

	UserCredential save(UserCredential userCredential);
}
