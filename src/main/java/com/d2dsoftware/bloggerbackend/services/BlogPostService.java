package com.d2dsoftware.bloggerbackend.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.d2dsoftware.bloggerbackend.domain.BlogPost;
import com.d2dsoftware.bloggerbackend.domain.utils.BlogPostCategory;
import com.d2dsoftware.bloggerbackend.domain.utils.Systems;

public interface BlogPostService {
	BlogPost save(BlogPost blogPost);

	BlogPost getById(String id);

	Page<BlogPost> getBySystem(Systems ownedSystem, Pageable pageable);

	Page<BlogPost> getAllPublishedBySystem(Systems ownedSystem, Pageable pageable);

	Page<BlogPost> getAllPublishedBySystemAndCategory(Systems ownedSystem, BlogPostCategory category,
			Pageable pageable);

	void deleteById(String id);
}
