package com.d2dsoftware.bloggerbackend.services;

import org.springframework.web.multipart.MultipartFile;

import com.mongodb.gridfs.GridFSDBFile;

public interface StorageService {
	String storeFile(MultipartFile file);

	GridFSDBFile getStoredFileByName(String fileName);
}
