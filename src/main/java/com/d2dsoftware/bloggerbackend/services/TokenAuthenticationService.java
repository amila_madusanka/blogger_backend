package com.d2dsoftware.bloggerbackend.services;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;

public interface TokenAuthenticationService {
	String getAuthenticationToken(String username);

	void addAuthentication(HttpServletResponse response, String username);

	Authentication getAuthentication(HttpServletRequest request);
}
