package com.d2dsoftware.bloggerbackend.domain;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import com.d2dsoftware.bloggerbackend.domain.utils.BlogPostCategory;
import com.d2dsoftware.bloggerbackend.domain.utils.Systems;

@Document(collection = "blogger_blog_posts_c")
public class BlogPost {

	@Id
	private String id;
	private String title;
	private String description;
	private String content;
	private BlogPostCategory category;
	private String coverImage;
	private Systems ownedSystem;
	private String creator;
	private boolean published;

	@CreatedDate
	private DateTime createdAt;

	@LastModifiedDate
	private DateTime updatedAt;

	public BlogPost() {
		super();
	}

	public BlogPost(Builder builder) {
		super();
		this.id = builder.id;
		this.title = builder.title;
		this.content = builder.content;
		this.category = builder.category;
		this.coverImage = builder.coverImage;
		this.ownedSystem = builder.ownedSystem;
		this.creator = builder.creator;
		this.published = builder.published;
		this.createdAt = builder.createdAt;
		this.updatedAt = builder.updatedAt;
		this.description = builder.description;
	}

	public String getId() {
		return this.id;
	}

	public String getTitle() {
		return this.title;
	}

	public String getDescription() {
		return this.description;
	}

	public String getContent() {
		return this.content;
	}

	public String getCoverImage() {
		return this.coverImage;
	}

	public BlogPostCategory getCategory() {
		return this.category;
	}

	public Systems getOwnedSystem() {
		return this.ownedSystem;
	}

	public String getCreator() {
		return this.creator;
	}

	public boolean isPublished() {
		return this.published;
	}

	public DateTime getCreatedAt() {
		return this.createdAt;
	}

	public DateTime getUpdatedAt() {
		return this.updatedAt;
	}

	public static class Builder {

		private String id;
		private String title;
		private String description;
		private String content;
		private BlogPostCategory category;
		private String coverImage;
		private Systems ownedSystem;
		private String creator;
		private boolean published;
		private DateTime createdAt;
		private DateTime updatedAt;

		public Builder id(String id) {
			this.id = id;
			return this;
		}

		public Builder title(String title) {
			this.title = title;
			return this;
		}

		public Builder description(String description) {
			this.description = description;
			return this;
		}

		public Builder content(String content) {
			this.content = content;
			return this;
		}

		public Builder coverImage(String coverImage) {
			this.coverImage = coverImage;
			return this;
		}

		public Builder category(BlogPostCategory category) {
			this.category = category;
			return this;
		}

		public Builder ownedSystem(Systems ownedSystem) {
			this.ownedSystem = ownedSystem;
			return this;
		}

		public Builder creator(String creator) {
			this.creator = creator;
			return this;
		}

		public Builder published(boolean published) {
			this.published = published;
			return this;
		}

		public Builder createdAt(DateTime createdAt) {
			this.createdAt = createdAt;
			return this;
		}

		public Builder updatedAt(DateTime updatedAt) {
			this.updatedAt = updatedAt;
			return this;
		}

		public BlogPost build() {
			return new BlogPost(this);
		}
	}
}
