package com.d2dsoftware.bloggerbackend.domain.utils;

public enum BlogPostCategory {
	LIFE_STYLE, FASHION, PROMOTIONS, ABOUT, INSPIRATION;
}
