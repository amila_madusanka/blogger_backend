package com.d2dsoftware.bloggerbackend.domain;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import com.d2dsoftware.bloggerbackend.domain.utils.Authority;

@Document(collection = "blogger_user_credentials_c")
public class UserCredential {

	@Id
	private String id;
	private String username;
	private String password;
	private Authority authority;
	private boolean enabled;

	@CreatedDate
	private DateTime createdAt;

	@LastModifiedDate
	private DateTime updatedAt;

	public UserCredential() {
		super();
	}

	public UserCredential(Builder builder) {
		super();
		this.id = builder.id;
		this.username = builder.username;
		this.password = builder.password;
		this.authority = builder.authority;
		this.enabled = builder.enabled;
		this.createdAt = builder.createdAt;
		this.updatedAt = builder.updatedAt;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

	public String getId() {
		return this.id;
	}

	public Authority getAuthority() {
		return this.authority;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public DateTime getCreatedAt() {
		return this.createdAt;
	}

	public DateTime getUpdatedAt() {
		return this.updatedAt;
	}

	public static class Builder {
		private String id;

		private String username;

		private String password;

		private Authority authority;

		private boolean enabled;

		private DateTime createdAt;

		private DateTime updatedAt;

		public Builder username(String username) {
			this.username = username;
			return this;
		}

		public Builder password(String password) {
			this.password = password;
			return this;
		}

		public Builder id(String id) {
			this.id = id;
			return this;
		}

		public Builder authority(Authority authority) {
			this.authority = authority;
			return this;
		}

		public Builder enabled(boolean enabled) {
			this.enabled = enabled;
			return this;
		}

		public Builder createdAt(DateTime createdAt) {
			this.createdAt = createdAt;
			return this;
		}

		public Builder updatedAt(DateTime updatedAt) {
			this.updatedAt = updatedAt;
			return this;
		}

		public UserCredential build() {
			return new UserCredential(this);
		}
	}
}
