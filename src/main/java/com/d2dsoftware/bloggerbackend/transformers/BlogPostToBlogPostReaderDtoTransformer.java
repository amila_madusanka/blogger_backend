package com.d2dsoftware.bloggerbackend.transformers;

import com.d2dsoftware.bloggerbackend.domain.BlogPost;
import com.d2dsoftware.bloggerbackend.dtos.BlogPostReaderDto;

public class BlogPostToBlogPostReaderDtoTransformer implements Transformer<BlogPost, BlogPostReaderDto> {

	@Override
	public BlogPostReaderDto transform(BlogPost source) {
		return new BlogPostReaderDto(source.getId(), source.getTitle(), source.getCoverImage(), source.getDescription(),
				source.getCreatedAt().toString());
	}

}
