package com.d2dsoftware.bloggerbackend.transformers;

import org.springframework.data.domain.Page;

import com.d2dsoftware.bloggerbackend.domain.BlogPost;
import com.d2dsoftware.bloggerbackend.dtos.BlogPostReaderDto;

public class BlogPostPageToBlogPostReaderDtoPageTransformer
		implements Transformer<Page<BlogPost>, Page<BlogPostReaderDto>> {

	@Override
	public Page<BlogPostReaderDto> transform(Page<BlogPost> source) {
		return source.map(blogPost -> new BlogPostToBlogPostReaderDtoTransformer().transform(blogPost));
	}

}
