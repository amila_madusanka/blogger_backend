package com.d2dsoftware.bloggerbackend.transformers;

import com.d2dsoftware.bloggerbackend.domain.BlogPost;
import com.d2dsoftware.bloggerbackend.dtos.BlogPostOutputDto;

public class BlogPostToBlogPostOutputDtoTransformer implements Transformer<BlogPost, BlogPostOutputDto> {

	@Override
	public BlogPostOutputDto transform(BlogPost source) {
		return new BlogPostOutputDto(source.getId(), source.getTitle(), source.getDescription(), source.getContent(),
				source.getCoverImage(), source.getCategory(), source.getOwnedSystem(), source.isPublished(),
				source.getCreatedAt().toString(), source.getUpdatedAt().toString());
	}

}
