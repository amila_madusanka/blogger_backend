package com.d2dsoftware.bloggerbackend.transformers;

import org.springframework.data.domain.Page;

import com.d2dsoftware.bloggerbackend.domain.BlogPost;
import com.d2dsoftware.bloggerbackend.dtos.BlogPostManagementDto;

public class BlogPostPageToBlogPostManagementDtoPageTransformer
		implements Transformer<Page<BlogPost>, Page<BlogPostManagementDto>> {

	@Override
	public Page<BlogPostManagementDto> transform(Page<BlogPost> source) {
		return source.map(blogPost -> new BlogPostToBlogPostManagementDtoTransformer().transform(blogPost));
	}

}
