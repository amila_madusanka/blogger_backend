package com.d2dsoftware.bloggerbackend.transformers;

import com.d2dsoftware.bloggerbackend.domain.BlogPost;
import com.d2dsoftware.bloggerbackend.dtos.BlogPostManagementDto;

public class BlogPostToBlogPostManagementDtoTransformer implements Transformer<BlogPost, BlogPostManagementDto> {

	@Override
	public BlogPostManagementDto transform(BlogPost source) {
		return new BlogPostManagementDto(source.getId(), source.getTitle(), source.getCoverImage(),
				source.getCategory(), source.isPublished(), source.getCreatedAt().toString(),
				source.getUpdatedAt().toString());
	}

}
