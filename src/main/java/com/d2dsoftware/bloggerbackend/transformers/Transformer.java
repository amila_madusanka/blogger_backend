package com.d2dsoftware.bloggerbackend.transformers;

public interface Transformer<S, T> {
	T transform(S source);
}
