package com.d2dsoftware.bloggerbackend.controllers;

import java.security.Principal;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.d2dsoftware.bloggerbackend.domain.UserCredential;
import com.d2dsoftware.bloggerbackend.dtos.AuthenticationInputDto;
import com.d2dsoftware.bloggerbackend.dtos.ChangePasswordInputDto;
import com.d2dsoftware.bloggerbackend.dtos.UserRegisterInputDto;
import com.d2dsoftware.bloggerbackend.dtos.UserRegisterOutputDto;
import com.d2dsoftware.bloggerbackend.security.TokenFactory;
import com.d2dsoftware.bloggerbackend.services.UserCredentialService;

@RestController
@RequestMapping("/blogger/authenticate")
@CrossOrigin
public class AuthenticationController {

	private final UserCredentialService userCredentialService;
	private final TokenFactory tokenFactory;

	@Autowired
	public AuthenticationController(final UserCredentialService userCredentialService,
			final TokenFactory tokenFactory) {
		super();
		this.userCredentialService = userCredentialService;
		this.tokenFactory = tokenFactory;
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> authenticate(@Validated @RequestBody AuthenticationInputDto authDto) {
		UserCredential credential = this.userCredentialService.getByUsernameAndPassword(authDto.getUsername(),
				authDto.getPassword());
		if (Objects.nonNull(credential))
			return ResponseEntity.ok(this.tokenFactory.generteOutputToken(credential));

		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("UnAuthorized");
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.CREATED)
	public @ResponseBody UserRegisterOutputDto register(
			@Validated @RequestBody UserRegisterInputDto userRegisterInput) {
		UserCredential savedCredential = this.userCredentialService.save(new UserCredential.Builder()
				.username(userRegisterInput.getUsername()).password(userRegisterInput.getPassword())
				.enabled(userRegisterInput.getEnabled()).authority(userRegisterInput.getAuthority()).build());
		return new UserRegisterOutputDto(savedCredential.getId());
	}

	@RequestMapping(value = "/cpwd", method = RequestMethod.PUT)
	@ResponseStatus(code = HttpStatus.OK)
	public @ResponseBody void changePassword(@Validated @RequestBody ChangePasswordInputDto changePasswordInput,
			Principal principal) {
		UserCredential existingCredential = this.userCredentialService.getByUsernameAndPassword(principal.getName(),
				changePasswordInput.getOldPassword());
		if (Objects.isNull(existingCredential))
			throw new ResourceNotFoundException();
		this.userCredentialService.save(
				new UserCredential.Builder().id(existingCredential.getId()).username(existingCredential.getUsername())
						.password(changePasswordInput.getNewPassword()).authority(existingCredential.getAuthority())
						.enabled(existingCredential.isEnabled()).createdAt(existingCredential.getCreatedAt()).build());
	}

}
