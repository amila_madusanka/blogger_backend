package com.d2dsoftware.bloggerbackend.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.d2dsoftware.bloggerbackend.dtos.StorageOutputDto;
import com.d2dsoftware.bloggerbackend.services.StorageService;
import com.mongodb.gridfs.GridFSDBFile;

@RestController
@RequestMapping("/blogger/storage")
@CrossOrigin
public class StorageController {

	private final StorageService storageService;

	@Value("${blogger.url}")
	private String serverUrl;

	@Autowired
	public StorageController(final StorageService storageService) {
		super();
		this.storageService = storageService;
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public StorageOutputDto createOrUpdate(@RequestParam("file") MultipartFile file) {
		return new StorageOutputDto(serverUrl + "/blogger/storage/public/" + this.storageService.storeFile(file));
	}

	@RequestMapping(path = "/public/{name:.+}", method = RequestMethod.GET)
	public HttpEntity<byte[]> get(@PathVariable("name") String name) throws IOException {
		GridFSDBFile loadedFile = this.storageService.getStoredFileByName(name);
		if (Objects.isNull(loadedFile))
			throw new ResourceNotFoundException();

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		loadedFile.writeTo(os);

		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, loadedFile.getContentType());
		return new HttpEntity<>(os.toByteArray(), headers);
	}
}
