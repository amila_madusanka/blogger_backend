package com.d2dsoftware.bloggerbackend.controllers;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.d2dsoftware.bloggerbackend.domain.BlogPost;
import com.d2dsoftware.bloggerbackend.domain.utils.BlogPostCategory;
import com.d2dsoftware.bloggerbackend.domain.utils.Systems;
import com.d2dsoftware.bloggerbackend.dtos.BlogPostOutputDto;
import com.d2dsoftware.bloggerbackend.dtos.BlogPostReaderDto;
import com.d2dsoftware.bloggerbackend.services.BlogPostService;
import com.d2dsoftware.bloggerbackend.transformers.BlogPostPageToBlogPostReaderDtoPageTransformer;
import com.d2dsoftware.bloggerbackend.transformers.BlogPostToBlogPostOutputDtoTransformer;

@RestController
@RequestMapping("/blogger/blog_posts/public")
@CrossOrigin
public class BlogPostForReadersController {

	private final BlogPostService blogPostService;

	@Autowired
	public BlogPostForReadersController(final BlogPostService blogPostService) {
		super();
		this.blogPostService = blogPostService;
	}

	@RequestMapping(value = "/{blogPostId}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public @ResponseBody BlogPostOutputDto getBlogPostById(@PathVariable String blogPostId) {
		BlogPost fetchedBlogpost = this.blogPostService.getById(blogPostId);
		if (Objects.isNull(fetchedBlogpost))
			throw new ResourceNotFoundException();

		return new BlogPostToBlogPostOutputDtoTransformer().transform(fetchedBlogpost);
	}

	@RequestMapping(value = "/{system}/reader/all", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public @ResponseBody Page<BlogPostReaderDto> getAllBlogPostsForReaders(@PathVariable Systems system,
			Pageable pageable) {
		return new BlogPostPageToBlogPostReaderDtoPageTransformer()
				.transform(this.blogPostService.getAllPublishedBySystem(system, pageable));
	}

	@RequestMapping(value = "/{system}/{category}/reader/all", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public @ResponseBody Page<BlogPostReaderDto> getAllBlogPostsByCategoryForReaders(@PathVariable Systems system,
			@PathVariable BlogPostCategory category, Pageable pageable) {
		return new BlogPostPageToBlogPostReaderDtoPageTransformer()
				.transform(this.blogPostService.getAllPublishedBySystemAndCategory(system, category, pageable));
	}
}
