package com.d2dsoftware.bloggerbackend.controllers;

import java.security.Principal;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.d2dsoftware.bloggerbackend.domain.BlogPost;
import com.d2dsoftware.bloggerbackend.domain.utils.Systems;
import com.d2dsoftware.bloggerbackend.dtos.BlogPostInputDto;
import com.d2dsoftware.bloggerbackend.dtos.BlogPostManagementDto;
import com.d2dsoftware.bloggerbackend.dtos.BlogPostOutputDto;
import com.d2dsoftware.bloggerbackend.services.BlogPostService;
import com.d2dsoftware.bloggerbackend.services.UserCredentialService;
import com.d2dsoftware.bloggerbackend.transformers.BlogPostPageToBlogPostManagementDtoPageTransformer;
import com.d2dsoftware.bloggerbackend.transformers.BlogPostToBlogPostOutputDtoTransformer;

@RestController
@RequestMapping("/blogger/blog_posts")
@CrossOrigin
public class BlogPostController {

	private final BlogPostService blogPostService;
	private final UserCredentialService userCredentialService;

	@Autowired
	public BlogPostController(final BlogPostService blogPostService,
			final UserCredentialService userCredentialService) {
		super();
		this.blogPostService = blogPostService;
		this.userCredentialService = userCredentialService;
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.CREATED)
	public @ResponseBody BlogPostOutputDto saveBlogPost(@Validated @RequestBody BlogPostInputDto blogPostInput,
			Principal principal) {
		BlogPost savedBlogPost = this.blogPostService.save(new BlogPost.Builder().title(blogPostInput.getTitle())
				.description(blogPostInput.getDescription()).content(blogPostInput.getContent())
				.coverImage(blogPostInput.getCoverImage()).category(blogPostInput.getCategory())
				.ownedSystem(blogPostInput.getOwnedSystem()).published(blogPostInput.isPublished())
				.creator(this.userCredentialService.getByUsername(principal.getName()).getId()).build());
		return new BlogPostToBlogPostOutputDtoTransformer().transform(savedBlogPost);
	}

	@RequestMapping(value = "/{blogPostId}", method = RequestMethod.PUT)
	@ResponseStatus(code = HttpStatus.OK)
	public @ResponseBody BlogPostOutputDto updateBlogPost(@Validated @RequestBody BlogPostInputDto blogPostInput,
			@PathVariable String blogPostId) {
		BlogPost existingBlogpost = this.blogPostService.getById(blogPostId);
		if (Objects.isNull(existingBlogpost))
			throw new ResourceNotFoundException();

		BlogPost updatedBlogPost = this.blogPostService
				.save(new BlogPost.Builder().title(blogPostInput.getTitle()).description(blogPostInput.getDescription())
						.content(blogPostInput.getContent()).category(blogPostInput.getCategory())
						.coverImage(blogPostInput.getCoverImage()).ownedSystem(blogPostInput.getOwnedSystem())
						.published(blogPostInput.isPublished()).creator(existingBlogpost.getCreator())
						.id(existingBlogpost.getId()).createdAt(existingBlogpost.getCreatedAt()).build());

		return new BlogPostToBlogPostOutputDtoTransformer().transform(updatedBlogPost);
	}
	
	@RequestMapping(value = "/{blogPostId}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public @ResponseBody BlogPostOutputDto getBlogPostById(@PathVariable String blogPostId) {
		BlogPost fetchedBlogpost = this.blogPostService.getById(blogPostId);
		if (Objects.isNull(fetchedBlogpost))
			throw new ResourceNotFoundException();

		return new BlogPostToBlogPostOutputDtoTransformer().transform(fetchedBlogpost);
	}

	@RequestMapping(value = "/{system}/management", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public @ResponseBody Page<BlogPostManagementDto> getAllBlogPostsForManagement(@PathVariable Systems system,
			Pageable pageable) {
		return new BlogPostPageToBlogPostManagementDtoPageTransformer()
				.transform(this.blogPostService.getBySystem(system, pageable));
	}

	@RequestMapping(value = "/{blogPostId}", method = RequestMethod.DELETE)
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteBlogPost(@PathVariable String blogPostId) {
		BlogPost existingBlogpost = this.blogPostService.getById(blogPostId);
		if (Objects.isNull(existingBlogpost))
			throw new ResourceNotFoundException();

		this.blogPostService.deleteById(blogPostId);
	}
}
